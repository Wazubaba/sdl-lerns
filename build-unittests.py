#!/usr/bin/env python

print("hi")
import os

LIBS = "lib/libinputvisitor.lib lib/libSDLang.lib lib/libDerelictUtil.lib lib/libDerelictSDL2.lib lib/libDerelictLua.lib lib/libjsonizer.lib lib/libdtiled.lib"
FILES = "src/engine/exceptions.d src/engine/font.d src/engine/graphics.d src/engine/image.d src/engine/info/package.d src/engine/info/posix.d src/engine/info/windows.d src/engine/init.d src/engine/loaders.d src/engine/package.d src/engine/scripting.d src/engine/sprite.d src/engine/text.d src/engine/types.d src/engine/ui/button.d src/engine/util/string.d src/engine/util.d src/engine/vdrive.d src/engine/window.d src/game/main.d"

def fgrep(filename, keyword):
    with open(filename, "r") as fp:
        data = fp.readlines()
        for datum in data:
            if datum.find(keyword) != -1:
                return True
    return False 

def getDeps(filename):
    deps = []
    with open(filename, "r") as fp:
        data = fp.readlines()
        for line in data:
            if line[:5] == "import" and line.find("engine") != -1:
                pass
            

def getFiles():
    files = ""
    for d, _, f in os.walk("src"):
        for fh in f:
            if fname.find("skip") == -1 and fname[-1] == "d":
                files += "%s/%s " %(d, fh)
    return files
        

def build(filename, output):
    os.system("dmd -w -g -Iinclude -Isrc -unittest -main -m32 -of%s %s %s" %(output, filename, LIBS))

for d, _, f in os.walk("src"):
    for fname in f:
        if fname.find("skip") == -1 and fname[-1] == "d":
            if fgrep("%s/%s" %(d,fname), "unittest"):
                fname = "%s/%s" %(d, fname)
                print("DD\t%s\t->unittests/%s" %(fname, fname[:-2]))
                build(fname, "unittests/%s" %(fname[:-2]))
                
