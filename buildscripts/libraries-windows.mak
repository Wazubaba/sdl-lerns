# Build D external libs for windows target

libs:lib/libinputvisitor.lib lib/libDerelictSDL2.lib lib/libDerelictLua.lib lib/libDerelictUtil.lib lib/libjsonizer.lib lib/libdtiled.lib lib/libSDLang.lib dlls

lib/libDerelictSDL2.lib: lib/libinputvisitor.lib lib/libDerelictUtil.lib
	@cd lib/derelict-sdl2; $(MAKE) -s $(MODE)

lib/libDerelictLua.lib: lib/libinputvisitor.lib lib/libDerelictUtil.lib
	@cd lib/derelict-lua; $(MAKE) -s $(MODE)

lib/libDerelictUtil.lib: lib/libinputvisitor.lib
	@cd lib/derelict-util; $(MAKE) -s $(MODE)

lib/libjsonizer.lib: lib/libinputvisitor.lib
	@cd lib/jsonizer; $(MAKE) -s $(MODE)

lib/libdtiled.lib: lib/libinputvisitor.lib lib/libjsonizer.lib
	@cd lib/dtiled; $(MAKE) -s $(MODE)

lib/libSDLang.lib: lib/libinputvisitor.lib lib/libDerelictUtil.lib
	@cd lib/sdlang-d; $(MAKE) -s $(MODE)

lib/libinputvisitor.lib:
	@echo "[Building Libraries..]" # Cheating by calling this first :P
	@cd lib/inputvisitor; $(MAKE) -s $(MODE)

zlib1.dll:
	cp $(SUPDIR)/zlib1.dll .

libpng16-16.dll:
	cp $(SUPDIR)/libpng16-16.dll .

libfreetype-6.dll:
	cp $(SUPDIR)/libfreetype-6.dll .

dlls: zlib1.dll libpng16-16.dll libfreetype-6.dll
