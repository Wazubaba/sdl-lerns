# Configuration for building a windows target

LIBS := lib/libinputvisitor.lib lib/libSDLang.lib lib/libDerelictUtil.lib lib/libDerelictSDL2.lib lib/libDerelictLua.lib lib/libjsonizer.lib lib/libdtiled.lib
LIBNAMES= lib/libinputvisitor.lib lib/libDerelictSDL2.lib lib/libDerelictLua.lib lib/libDerelictUtil.lib lib/libjsonizer.lib lib/libdtiled.lib lib/libSDLang.lib
PLATFORM_OBJECT_TYPE= obj

ifeq ($(shell uname -m), x86_64)
LINK_PATH= $(LIBS) #support/win32/SDL2.lib # support/win32/lua53 # FIXME: change this to win64
SUPDIR= support/win32
endif
ifeq ($(shell uname -m), x86)
LINK_PATH= $(LIBS) #support/win32/SDL2 support/win32/lua53
SUPDIR= support/win32
endif

platform_install: bin/$(BIN)
	@echo "TODO: figure out how to do this properly on windows"

platform_uninstall:
	@echo "TODO: figure out how to do this properly on windows"
