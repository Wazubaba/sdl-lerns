# Build D external libs for linux target

libs:lib/libinputvisitor.a lib/libDerelictSDL2.a lib/libDerelictLua.a lib/libDerelictUtil.a lib/libjsonizer.a lib/libdtiled.a lib/libSDLang.a

lib/libDerelictSDL2.a: lib/libinputvisitor.a lib/libDerelictUtil.a
	@cd lib/derelict-sdl2; $(MAKE) -s $(MODE)

lib/libDerelictLua.a: lib/libinputvisitor.a lib/libDerelictUtil.a
	@cd lib/derelict-lua; $(MAKE) -s $(MODE)

lib/libDerelictUtil.a: lib/libinputvisitor.a
	@cd lib/derelict-util; $(MAKE) -s $(MODE)

lib/libjsonizer.a: lib/libinputvisitor.a
	@cd lib/jsonizer; $(MAKE) -s $(MODE)

lib/libdtiled.a: lib/libinputvisitor.a lib/libjsonizer.a
	@cd lib/dtiled; $(MAKE) -s $(MODE)

lib/libSDLang.a: lib/libinputvisitor.a lib/libDerelictUtil.a
	@cd lib/sdlang-d; $(MAKE) -s $(MODE)

lib/libinputvisitor.a:
	@echo "[Building Libraries..]" # Cheating by calling this first :P
	@cd lib/inputvisitor; $(MAKE) -s $(MODE)
