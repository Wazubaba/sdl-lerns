# Configuration for building a linux target

LIBS := -L-linputvisitor -L-lSDLang -L-lDerelictUtil -L-lDerelictSDL2 -L-lDerelictLua -L-ljsonizer -L-ldtiled
LIBNAMES= lib/libinputvisitor.a lib/libDerelictSDL2.a lib/libDerelictLua.a lib/libDerelictUtil.a lib/libjsonizer.a lib/libdtiled.a lib/libSDLang.a
PLATFORM_OBJECT_TYPE= o

ifeq ($(shell uname -m), x86_64)
LINK_PATH= -L-Lsupport/lin64
endif
ifeq ($(shell uname -m), x86)
LINK_PATH= -L-Lsupport/lin32
endif

platform_install: bin/$(BIN)
	@mkdir -p $(DESTDIR)/usr/local/bin
	@cp bin/$(BIN) $(DESTDIR)/usr/local/bin/$(BIN)
	@echo "Installed $(BIN) to $(DESTDIR)/usr/local/bin"

platform_uninstall:
	@-$(RM) $(DESTDIR)/usr/local/bin/$(BIN)
	@echo "Deleted $(BIN)"
