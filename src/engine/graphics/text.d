module engine.graphics.text;

import derelict.sdl2.sdl: SDL_Surface, SDL_RenderCopy, SDL_Color, SDL_Texture, SDL_CreateTextureFromSurface, SDL_FreeSurface, SDL_Rect;
import derelict.sdl2.ttf: TTF_Font, TTF_OpenFont, TTF_CloseFont, TTF_RenderUTF8_Solid, TTF_RenderUTF8_Shaded, TTF_RenderUTF8_Blended, TTF_GetError;

import std.string: fromStringz, toStringz;

import engine.graphics.font: Font, LoadFont;
import engine.core.types: Vec2i, RGB, Rect;
import engine.cli: Perror, Print, Pinfo;
import engine.graphics.window: Window;
import engine.graphics.sprite: Sprite;
import engine.graphics.image: Image;
import engine.core.exceptions: UninitializedResourceError;

/*
	TODO:
		implement basic text processing and switch to a multi-text line paradigm;
		newlines should append a second line of text to the construct, and it needs
		to be drawn easilly.

		We can use the new Renderer.BatchDraw() function to easilly draw an array of
		lines.
*/

enum TextStyle
{
	Solid,
	Shaded,
	Blended
}

/++
	Text that is drawn to the screen.
++/
class Text
{
	RGB color;
	string text;

	Font font;
	Sprite reference;
	Image rawSurface;

	@property Rect locality(){ return this.reference.locality; }
	@property void locality(Rect rect){ this.reference.locality = rect; }

/++
	Set a few internal values, nothing else.
++/
	this(ref Font font, const RGB color, string text = "")
	{
		if (font.reference == null)
			throw new UninitializedResourceError;
		this.font = font;
		this.color = color;
		this.text = text;
	}

/++
	Due to how SDL works, we have to bake the text to a surface, then to a
	texture. It is absolutely imperative that you call this before trying
	to draw text, otherwise you will be handing a null pointer to the
	renderer :P

	The bg argument is only needed if you specify the style as TextStyle.Shaded.
++/
	void Bake(ref Window window, const TextStyle style = TextStyle.Solid, const RGB bg = RGB(0x22, 0x22, 0x22))
	{
		this.rawSurface = new Image();
		final switch (style) with (TextStyle)
		{
			case Solid:
				this.rawSurface.reference = TTF_RenderUTF8_Solid(font.reference, this.text.toStringz, this.color.SDLify);
				break;

			case Shaded:
				this.rawSurface.reference = TTF_RenderUTF8_Shaded(font.reference, this.text.toStringz, this.color.SDLify, bg.SDLify);
				break;

			case Blended:
				this.rawSurface.reference = TTF_RenderUTF8_Blended(font.reference, this.text.toStringz, this.color.SDLify);
				break;
		}
		this.reference = new Sprite(this.rawSurface, window);
		import std.stdio:writeln;
		writeln(TTF_GetError().fromStringz);
	}
}
