module engine.graphics.font;
import derelict.sdl2.ttf: TTF_Font, TTF_OpenFont, TTF_CloseFont, TTF_GetError;
import engine.io.vdrive;
import engine.core.exceptions: FileLoadError;

import std.string: toStringz, fromStringz;

/++
	This is a container for a ttf font loaded from disk.
++/
class Font
{
	TTF_Font* reference = null;

	this(){}

	~this()
	{
		TTF_CloseFont(this.reference);
	}

	/++
		Loads a font from a given path with the given size. No checks are
		performed on this path, so ensure you properly test it if possible.
		Throws: FileLoadError if the given file cannot be opened.
	++/
	void LoadFromPath(string path, int size)
	{
		import std.stdio: write, stdout;
		write("[font] Loading ", path, "..."); stdout.flush();

		this.reference = TTF_OpenFont(path.toStringz, size);
		if (!this.reference)
		{
			write("\tFAIL!\n"); stdout.flush();
			throw new FileLoadError(path, cast(string) TTF_GetError().fromStringz);
		}
		write("\tsuccess!\n"); stdout.flush();
	}
}

/++
	UFCS wrapper extension for VirtualFileSystem support.
	Throws: FileLoadError if file cannot be opened.
	Returns: a Font class variable.
++/
Font LoadFont(VirtualFileSystem vfs, string path, int size)
{
	if (!vfs.HasFile(path))
		throw new FileLoadError(path);

	Font font = new Font();
	font.LoadFromPath(vfs.GetPathToFile(path), size);
	return font;
}
