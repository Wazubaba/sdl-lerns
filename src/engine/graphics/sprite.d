module engine.graphics.sprite;
import engine.graphics.image: Image;
import engine.graphics.window: Window;
import engine.core.exceptions: ResourceInitializationError, UninitializedResourceError;
import engine.core.types: Vec2i, Rect;
import derelict.sdl2.sdl: SDL_Texture, SDL_DestroyTexture, SDL_CreateTextureFromSurface, SDL_Renderer, SDL_GetError;

import std.string: fromStringz, toStringz;
/++
	Provides a useful abstraction above an SDL_Texture. Has a position, size,
	and clipping area which controls what part of the image to draw, how large
	to draw it, and where to draw it on the window. With this you do not needed
	to call any extra functions after creation.

	Params:
		reference =   - a reference to the wrapped SDL_Texture
		position =    - position on the screen to draw at
		size =        - size to draw the image as
		clip =        - sub-area of the image to draw
		clipsize =    - size of the sub-area to draw

++/
class Sprite
{
	SDL_Texture* reference = null;

	Rect locality; // TODO: actually make use of this
	Rect region;

	/++
		Load an Image from a given path and create a texture for a given renderer.
		Throws: SpriteCreationError on failure
	++/
	this(string path, ref Window window)
	{
		Image image = new Image(path, window);
		this.reference = SDL_CreateTextureFromSurface(window.context, image.reference);

		this.locality.size = Vec2i(image.reference.w, image.reference.h);
		this.region = Rect(0, 0, image.reference.w, image.reference.h);

		if (!this.reference) throw new ResourceInitializationError("Sprite", SDL_GetError().fromStringz.dup);
	}
	///
	unittest
	{
		import engine.graphics.window: Window;

		// Create a new window
		auto window = new Window("Texture module unittest", 640, 480);

		// Create a new texture
		auto sprite = new Sprite("data/image.png", window);
	}

	/++
		Create a texture from the given image for the given renderer.
		Throws: SpriteCreationError on failure
	++/
	this(ref Image existingImage, ref Window window)
	{
		if (existingImage is null) throw new UninitializedResourceError;

		this.reference = SDL_CreateTextureFromSurface(window.context, existingImage.reference);
		this.locality.size = Vec2i(existingImage.reference.w, existingImage.reference.h);
		this.region = Rect(0, 0, existingImage.reference.w, existingImage.reference.h);

		if (!this.reference) throw new ResourceInitializationError("Sprite", SDL_GetError().fromStringz.dup);
	}
	///
	unittest
	{
		import engine.graphics.window: Window;
		import engine.graphics.image: Image;

		// Create a new window
		auto window = new Window("Texture module unittest", 640, 480);
		// Create a new image
		auto i = new Image("data/image.png", window);

		// Create a new texture from the previous image
		auto t = new Sprite(i, window);
	}

	/// Deallocate the SDL_Texture.
	~this()
	{
		SDL_DestroyTexture(this.reference);
	}
}
