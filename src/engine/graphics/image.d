module engine.graphics.image;
import derelict.sdl2.sdl: SDL_Surface, SDL_ConvertSurface, SDL_FreeSurface, SDL_GetError;
import derelict.sdl2.image: IMG_Load, IMG_GetError;

import engine.core.init: DATA_PATHS;
import engine.graphics.window: Window;
//import engine.exceptions: ImageNotFound, ImageNotLoaded, ImageCreationError,
import engine.core.exceptions: ResourceInitializationError, FileNotFound, ConversionError, FileLoadError;
import engine.core.types: Vec2i, Rect;
import engine.io.vdrive: VirtualFileSystem;

import std.path: buildPath, isFile, exists;
import std.string: toStringz, fromStringz;
import std.stdio: write, writeln, stdout;

/*
	Simplify this down to a basic wrapper around an SDL_Image* and move all important functionality
	to Texture. Rename Texture to Sprite. Perhaps even see if we can remove any need for this module
	to exist.

	From now on, all access should /really/ be done via VFS access...
	..Which should be inside the loaders module and not at all in here. :|

	Given how simple text is, it may be a nice idea to just merge text into this upon refactor.
	After-all, a sprite can be anything graphical on the screen...
*/

/++
	Provides a useful abstraction above an SDL_Surface. Has a position, size,
	and clipping area which controls what part of the image to draw, how large
	to draw it, and where to draw it on the window.

	If you create an image manually, it is highly recommended that you also
	call Optimize manually as well, which optimizes the surface to the given
	window.

	Params:
		reference =   - a reference to the wrapped SDL_Surface
		position =    - position on the screen to draw at
		size =        - size to draw the image as
		clip =        - sub-area of the image to draw
		clipsize =    - size of the sub-area to draw
		isOptimized = - whether the image has been optimized for a given surface or not
++/
class Image
{
	SDL_Surface* reference = null;

	/++
		Just initialize; Useful for when you are generating an image, such as in the text class.
	++/
	this() {}

	/++
		Loads an image from the given path and performs optimization for the
		given window.
		Throws: ImageNotFound if the image does not exist
		Throws: ImageNotLoaded if an attempt to optimize an unloaded image happens
	++/
	this (string path, ref Window window)
	{
		string target;
		if (DATA_PATHS.length > 0)
		{
			foreach (dir; DATA_PATHS)
			{
				target = buildPath(dir, path);
				if (target.exists) break;
			}
		}
		else
		{
			target = path;
		}

		writeln("DEBUG: ",target);

		write("[image] Loading ", path, "..."); stdout.flush();

		if (!path.exists || !path.isFile)
			throw new FileNotFound(path);

		this.reference = IMG_Load(path.toStringz);

		if (!this.reference)
		{
			write("\tFAIL\n"); stdout.flush();
			throw new FileLoadError(path, IMG_GetError().fromStringz.dup);
		}
		else
			write("\tsuccess!\n"); stdout.flush();

//		this.reference = SDL_ConvertSurface(this.reference, window.windowSurface.format, 0); // FIXME: See note in src/engine/window.d
		if (!this.reference) throw new ResourceInitializationError("Image", SDL_GetError().fromStringz.dup);
	}
	///
	unittest
	{
		// Create a new window
		auto window = new Window("Image module unittest", 640, 480);

		// Create a new image optimized for the previous window
		auto i = new Image("data/image.png", window);
		assert (i !is null);
	}

	/// Deallocate the SDL_Surface.
	~this()
	{
		SDL_FreeSurface(this.reference);
	}
}
