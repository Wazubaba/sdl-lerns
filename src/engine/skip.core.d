module engine.core;
/**
	This module basically acts as the gatekeeper to all other subsystems.
	While they /can/ be called manually, it is best to call them through
	this module's interface.
**/
import derelict.sdl2.sdl;
import engine.window;
import engine.image;
/*
Window* ROOT_WINDOW = null;
string[] DATA_PATHS;

/// Generic reference containers
struct Container(T)
{
	T Reference;
	string name;
}

struct ContainerDB(T)
{ // Stores an array of a specific type and offers helper functions
	T[] database;
	T GetByName(string name)
	{
		foreach (T item; this.database)
		{
			if (item.name == name)
				return item;
		}
		return null;
	}

	T GetReferenceByName(string name)
	{
		foreach (T item; this.database)
		{
			if (item.name == name)
				return item.Reference;
		}
		return null;
	}

	@property size_t length() { return this.database.length; }

	void popByName(string name)
	{
		for (size_t i = 0; i < this.database.length; i++)
		{
			if (this.database[i].name == name)
				this.database = purge(this.database, i);
		}
	}
}


// Reference containers
Container!Window[] WINDOWS;
Container!Image[] TEXTURES;

Window OpenWindow(const string name, const string title, const int width, const int height, const int xpos = SDL_WINDOWPOS_UNDEFINED, const int ypos = SDL_WINDOWPOS_UNDEFINED)
{
	WINDOWS.length += 1;
	WINDOWS[$].name = name;
	WINDOWS[$].Reference = new Window(title, width, height, xpos, ypos);
	if (WINDOWS.length == 1) ROOT_WINDOW = WINDOWS.LENGTH[0].ptr;
	return WINDOWS[$];
}
*/
