module engine.animatedTexture;
import engine.image: Image;
import engine.renderer: Renderer;
import engine.exceptions: TextureCreationError;
import engine.types: Vec2i;
import derelict.sdl2.sdl: SDL_Texture, SDL_DestroyTexture, SDL_CreateTextureFromSurface, SDL_Renderer;

// This magical fucking line makes global variables work.
//import engine.init: ROOT_RENDERER;
/*
/++
	This class wraps around an SDL_Surface*. It can have a size and a position.
	Upon initialization, the specified image file will be loaded, and if an
	engine.init function `SetWindowAsRoot` is called, or ROOT_WINDOW is
	otherwise defined, it will automatically call the `Optimize` member.
	Alternatively, you can call it yourself later on.
++/
class AnimatedTexture
{
	SDL_Texture* reference = null;
	size_t frame;

	/+
		TODO: Brainstorm on moving all location and clip changing into the
		texture and image classes, and make these animated versions a wrapper
		around those classes. Rework window and renderer to grab the position,
		size, and clip information directly from the texture and image class
		references.
	+/

	/// Return a pointer to the underlying texture
	@property ref SDL_Texture* opCall() { return this.reference; }

	/++
		Automatically calls the needed members to load an image from a given
		path.
	++/
	// this(const string path)
	// {
	// 	if (!ROOT_RENDERER) throw new TextureCreationError;
	// 	Image imageData = new Image(path);
	// 	this.reference = SDL_CreateTextureFromSurface(ROOT_RENDERER.Reference, imageData.reference);
	// }
	//
	// this(Image image)
	// {
	// 	if (!ROOT_RENDERER) throw new TextureCreationError;
	// 	this.reference = SDL_CreateTextureFromSurface(ROOT_RENDERER.Reference, image.reference);
	// 	if (!this.reference) throw new TextureCreationError;
	// }

	this(const string path, const Vec2i clipSize, Renderer renderer)
	{
		Image imageData = new Image(path);
		this.clipSize = clipSize;
		this.reference = SDL_CreateTextureFromSurface(renderer.Reference, imageData.reference);
		this.size = Vec2i(imageData.reference.w, imageData.reference.h);
		if (!this.reference) throw new TextureCreationError;
	}

	this(Image image, const Vec2i clipSize, Renderer renderer)
	{
		this.clipSize = clipSize;
		this.reference = SDL_CreateTextureFromSurface(renderer(), image.reference);
		this.size = Vec2i(image.reference.w, image.reference.h);
		if (!this.reference) throw new TextureCreationError;
	}

	this(const string path, const Vec2i clipSize, SDL_Renderer* renderer)
	{
		Image imageData = new Image(path);
		this.clipSize = clipSize;
		this.reference = SDL_CreateTextureFromSurface(renderer, imageData.reference);
		this.size = Vec2i(imageData.reference.w, imageData.reference.h);
		if (!this.reference) throw new TextureCreationError;
	}

	this(Image image, const Vec2i clipSize, SDL_Renderer* renderer)
	{
		this.clipSize = clipSize;
		this.reference = SDL_CreateTextureFromSurface(renderer, image.reference);
		this.size = Vec2i(image.reference.w, image.reference.h);
		if (!this.reference) throw new TextureCreationError;
	}

	/// Deallocate the SDL_Surface.
	~this()
	{
		SDL_DestroyTexture(this.reference);
	}
}
*/
