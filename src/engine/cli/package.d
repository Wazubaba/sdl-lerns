module engine.cli;

/**
	Helper module to load the proper output lib for the platform.
**/

version (Posix)
{ // We can use nice, simple, ansi escape codes
	public import engine.cli.posix;
}
version (Windows)
{ // We have to jump through hoops and it isn't like average windows users use terminals anyway :/
	public import engine.cli.windows;
}
