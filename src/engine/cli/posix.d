module engine.cli.posix;
// Alex Hairy Man: debug is a keyword.
// Wazubaba: Oh, so I am a freaking idiot. Understood. *hits self on head*
import std.stdio: writeln, writefln, write;
import core.vararg;

/**
	Global debugging level. Any messages with a priority lower(greater than)
	this amount are not displayed.
**/
byte DEBUG_LEVEL = 0;

/// This is just to clear all special colors and stuff to the terminal default
static this()
{
	write("\x1b[0m");
}

/// This is just to clear all special colors and stuff to the terminal default
static ~this()
{
	write("\x1b[0m");
}

/// Wrapper for writeln that takes a debug level
void Print(S...)(const byte level, S args)
{
	if (level <= DEBUG_LEVEL)
	{
		writeln(args);
	}
}

/// Wrapper for writefln that takes a debug level
void Printf(S...)(const byte level, const string format, S args)
{
	if (level <= DEBUG_LEVEL)
	{
		writefln(format, args);
	}
}

/// Prints a bold red line prefixed with a bold red [ERROR]
void Perror(const string message, const byte level = 0)
{
	Print(level, "\x1b[31m[\x1b[1;41mERROR\x1b[0m\x1b[31m] ", message, "\x1b[0m");
}

/// Variadic version of Perror.
void Perror(S...)(const byte level, S args)
{
	Print(level, "\x1b[1;37m", "[", "\x1b[31m", "ERROR", "\x1b[1;37m", "] ", args, "\x1b[0m");
}

/// Formatted version of Perror.
void Ferror(S...)(const byte level, const string format, S args)
{
	string formline = "\x1b[1;37m[\x1b[31mERROR\x1b[1;37m] ";
	formline ~= format;
	formline ~= "\x1b[0m";
	Printf(level, formline, args);
}

/// Prints a simple line prefixed with [INFO]
void Pinfo(const string message, const byte level = 0)
{
	Print(level, "[INFO] ", message);
}

/// Variadic version of Pinfo.
void Pinfo(S...)(const byte level, S args)
{
	Print(level, "\x1b[0m","[INFO] ", args);
}

/// Formatted version of Pinfo.
void Finfo(S...)(const byte level, const string format, S args)
{
	string formline = "\x1b[0m[INFO] ";
	Printf(level, formline ~= format, args);
}

/// Prints a simple line prefixed with a yellow [WARN]
void Pwarn(const string message, const byte level = 0)
{
	Print(level, "\x1b[0m[\x1b[33mWARN\x1b[0m] ", message);
}

/// Variadic version of Pwarn.
void Pwarn(S...)(const byte level, S args)
{
	Print(level, "\x1b[0m[\x1b[33mWARN\x1b[0m] ", args);
}

/// Formatted version of Pwarn.
void Fwarn(S...)(const byte level, const string format, S args)
{
	string formline = "\x1b[0m[\x1b[33mWARN\x1b[0m] ";
	Printf(level, formline ~= format, args);
}
