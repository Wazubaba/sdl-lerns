module engine.config;

/**
	Provides a wrapper around an SDL-formatted config file.
**/

import sdlang;
import engine.info;

void TestSDL(string fileName)
{
	Print("Reading configuration from ", fileName);
	Tag root;
	try
	{
		root = parseSource(`
			welcome "Hello, world!"

			// Uncomment this for an error:
			// badSuffix 12Q

			myNamespace:person name="Joe Coder" {
				age 36
			}
		`);
	}
	catch (SDLangParseException e)
	{
		Perror(e.msg);
	}

	Value welcome = root.tags["welcome"][0].values[0];
	Print(welcome);
}