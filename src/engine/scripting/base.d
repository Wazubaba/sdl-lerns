module engine.scripting.base;
import engine.cli: Perror, Print, Pinfo;
import engine.core.exceptions: FileLoadError;

import std.string: toStringz, fromStringz;
import derelict.lua.lua;

import std.stdio: stdout;

alias luaState = lua_State*;

/++
	Exception thrown when a lua script reports an error.
++/
class LuaError: Exception
{
	@safe pure nothrow this(string msg)
	{
		super(msg);
	}
}

extern(C) int Test(lua_State* l) nothrow
{
	import std.stdio: writeln;

	int numArgs = lua_gettop(l);
	for (int i = 1; i <= numArgs; i++)
		try
			writeln(lua_tostring(l, i).fromStringz);
		catch (Exception e) {}

	return 0;
}

extern(C) int ErrorTest(lua_State* l) nothrow
{
	import std.stdio: writeln;
	lua_pushstring(l, "Here's your free error!");
	lua_error(l);

	return -1;
}

class LuaScript
{
	lua_State* state = null;
	int lastResult;

	~this()
	{
		lua_close(state);
	}

	this(string path)
	{
		Open(path);
	}

	this() {}

	void AddFunction(string funcName, lua_CFunction func)
	{
		lua_pushcfunction(this.state, func);
		lua_setglobal(this.state, funcName.toStringz);
	}

	void Open(string path)
	{
		import std.stdio: writeln;
		writeln("Loading script: '", path, "'..."); stdout.flush();

		this.state = luaL_newstate();
		luaL_openlibs(this.state);

		AddFunction("KaratonTestFunction", &Test);
		AddFunction("ErrorTest", &ErrorTest);

		this.lastResult = luaL_loadfile(this.state, path.toStringz);

		if (this.lastResult)
		{
			throw new FileLoadError(path);
		}
	}

	void Execute()
	{
		this.lastResult = lua_pcall(this.state, 0, LUA_MULTRET, 0);
		if (this.lastResult)
		{
			import std.string: format;
			throw new LuaError("%s".format(lua_tostring(this.state, -1).fromStringz));
		}
	}
}

//void print(const string s, const bool v){}
/*
int LoadScriptFromFile(const string path, const bool verbose = true)
{
	Pinfo("yo");
//	auto L = da_lua_newstate();
	lua_State* L; Pinfo("Made a lua state!");
	L = luaL_newstate(); Pinfo("Created new state!");
	luaL_openlibs(L); Pinfo("Loading base libs!");
	int status = luaL_loadfile(L, cast(const char*) path); Pinfo("Loading file!");
	if (status)
	{
		Pinfo("Could not find script.lua!");
		return status;
	}

	status = lua_pcall(L, 0, LUA_MULTRET, 0); Pinfo("Attempting call!");
	if (status)
	{
		Pinfo("AUGH!");
		Pinfo("AIIIIEEEEGHHH");
		return status;
	}

	lua_close(L);
	return 0;
}
*/
