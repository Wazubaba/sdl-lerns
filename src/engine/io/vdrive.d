module engine.io.vdrive;
import std.stdio;
import std.file;
import std.path;
import std.array;
import std.algorithm.mutation;
import std.algorithm.searching;

// FIXME: Cull unneeded imports asap

/++
	Provides a simple virtual drive abstracted over the normal file system.

	Params:
		string basePath = -   directory to create new files in. You can set this as needed.
++/
struct VirtualFileSystem
{
	string basePath = "";
	string[] mounts;
	bool mounted = false;

	/++
		Mount a directory as part of the filesystem.

		The first registered mount is where written files will be sent to.
		You can optionally specify what you want the $(B basePath) to be.

		Returns: true if the directory exists, else false.
	++/
	bool Mount(const string path, const string newBasePath = "")
	{

		if (this.basePath == "") this.basePath = newBasePath;
		if (exists(path) && !isFile(path))
		{
			if (this.basePath == "") this.basePath = path;
			this.mounts ~= path;
			this.mounted = true;
			return true;
		}
		return false;
	}
	///
	unittest
	{
		// Create a new virtual filesystem
		auto vfs = VirtualFileSystem();

		// Mount the data directory
		assert(vfs.Mount("data")); // Sets basePath automatically
	}

	/++
		Unmount a directory path from the filesystem.

		If the $(B basePath) is the target directory then it will be swapped to
		whatever directory was mounted as or becomes first in the mounts array.
	++/
	void UnMount(const string path)
	{
		if (mounts.find(path))
		{
			mounts.remove(path);

			if (path == basePath)
			{
				if (mounts.length < 2)
					basePath = "";
				else
					basePath = mounts[0];
			}
		}
	}
	///
	unittest
	{
		// Create a new virtual filesystem
		auto vfs = VirtualFileSystem();

		// Mount the data directory
		assert(vfs.Mount("data"));

		// Unmount the data directory
		vfs.UnMount("data"); // Unmount the data directory and sets basePath to "" due to no more mounts existing
	}

	/++
		This is an internally-used function for testing whether a path
		can exist within the bounds of the file system.

		Returns:
			true if it can, otherwise false
	++/
	bool testPath(const string path)
	{
		// XXX: if .. is found in the path it is invalid
		// TODO BUG: This has a bug where one could still access outside of the base dir
		// by specifying something such as "../hiIamOutsideTheBaseDirNow/yo"
		// TODO: find a way to ensure that we will always be inside the base dir
		/*
			This works based off a "points" approach. 0 is the root,
			and 1 point is added for each additional depth.

			Rules are:
				/ as char 0 means root
				/ can be omitted at the beginning of a path string
		*/
		foreach (string subpath; path.split("/"))
			if (subpath == "..") return false;
		return true;
		/* // Disabled as tbh idk if it is really needed. Is untested but ought to work.
		int points = 0;
		string[] worker = path.split("/");
		foreach (string subpath; worker)
		{
			if (subpath == "..")
				points--;
			else
			if (subpath.length > 0)
				points++;

			if (points < 0)
				return false;
		}

		return true;
		*/
	}

	/++
		Open a File within the filesystem with the specified mode at the given path.

		If you are creating a new file it will be created relative to the $(B basePath).

		Returns:
			Either a valid file pointer or a non-initialized one. Use File.isOpen()
			to test this.
	++/
	File Open(const string path, const string mode)
	{
		// TODO: figure out if we should be using exceptions or not here. Kind of unsure.
		import std.string: inPattern;
		File fp;
		if (testPath(path))
		{
			if (inPattern('r', mode))
				foreach (string subpath; mounts)
				{
					if (exists(buildPath(subpath, path)) && isFile(buildPath(subpath, path)))
					{
						fp = File(buildPath(subpath, path), mode);
						break;
					}
				}
			else
				fp = File(buildPath(basePath, path), mode);
			return fp;
		}
		return File();
	}
	///
	unittest
	{
		import std.stdio: File;

		// Create a new virtual filesystem
		auto vfs = VirtualFileSystem();

		// Mount the data directory
		assert(vfs.Mount("data"));

		// Attempt to open a file named script.lua in the data directory for reading
		auto script = vfs.Open("script.lua", "r"); // Open data/script.lua in read mode

		// Ensure it is actually open and not an invalid file
		assert(script.isOpen());
	}

	/++
		Tests if a file exists in the filesystem.
		Returns: true if the file exists otherwise false
	++/
	bool HasFile(const string path)
	{
		foreach (string subpath; mounts)
			if (exists(buildPath(subpath, path)) && isFile(buildPath(subpath, path)))
				return true;
		return false;
	}
	///
	unittest
	{
		// Create a new virtual filesystem
		auto vfs = VirtualFileSystem();

		// Mount the data directory
		assert(vfs.Mount("data"));

		// Test if a file named script.lua exists
		assert(vfs.HasFile("script.lua"));
	}

	/++
		Returns a localised path to a file.
		Returns: string containing results, or "" if the target does not exist
	++/
	string GetPathToFile(const string path)
	{
		foreach (string subpath; mounts)
		{
			string target = buildPath(subpath, path); // Only run this function once
			if (exists(target) && isFile(target))
				return target;
		}
		return "";
	}
	///
	unittest
	{
		// Create a new virtual filesystem
		auto vfs = VirtualFileSystem();

		// Mount the data directory
		assert(vfs.Mount("data"));

		// Test if script.lua is in data directory
		assert(vfs.GetPathToFile("script.lua") == "data/script.lua");
	}
}


unittest
{
	import std.stdio: File;
	auto vfs = VirtualFileSystem();
	assert(vfs.Mount("data"));
	assert(vfs.Mount("tools"));
	assert(!vfs.Mount("bagels"));
	assert(vfs.basePath == "data");

	// Test for opening of files
	{ // Tests a file inside the first mount
		auto fp = vfs.Open("script.lua", "r");
		assert(fp.isOpen());
	}
	{ // Tests a file inside the second mount
		auto fp = vfs.Open("tiled-0.16.0.tar.gz", "r");
		assert(fp.isOpen());
	}

	// Test for writing a new file
	{
		auto fp = vfs.Open("nya.test", "w");
		assert(fp.isOpen());
		fp.write("hi hi \\o");
		fp.close();
	}
}
