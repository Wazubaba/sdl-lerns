module engine.io.loaders;

import engine.graphics.image: Image;
import engine.graphics.window: Window;
import engine.graphics.sprite: Sprite;
import engine.io.vdrive: VirtualFileSystem;
import engine.scripting.base: LuaScript;
import engine.core.exceptions: FileNotFound;

/++
	Provides a UFCS extension layer to the virtual file system to simplify loading assets.
++/

/++
	Load an Image from a virtual filesystem.

	Throws: FileNotFound if a file does not exist
	Returns: A valid Image
++/
Image LoadImage(VirtualFileSystem vfs, string path, Window window)
{
	if (vfs.HasFile(path))
		return new Image(vfs.GetPathToFile(path), window);
	else throw new FileNotFound(path);
//	return null;
}

/++
	Load a Sprite from a virtual filesystem.

	Throws: FileNotFound if a file does not exist
	Returns: A valid Sprite
++/
Sprite LoadSprite(VirtualFileSystem vfs, string path, Window window)
{
	if (vfs.HasFile(path))
		return new Sprite(vfs.GetPathToFile(path), window);
	else throw new FileNotFound(path);
//	return null;
}

/++
	Load a LuaScript from a virtual filesystem.

	Throws: FileNotFound if a file does not exist
	Returns: A valid LuaScript
++/
LuaScript LoadScript(VirtualFileSystem vfs, string path)
{
	if (vfs.HasFile(path))
		return new LuaScript(vfs.GetPathToFile(path));
	else throw new FileNotFound(path);
//	return null;
}
