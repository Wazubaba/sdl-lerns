module engine.window;

import derelict.sdl2.sdl;
import engine.types;
import engine.image;
/*
	TODO: Turn this into a basic SDL_Window* container and move all draw control to the renderer. Rename renderer
	to Window.
*/

/++
	An OO wrapper for SDL_Window that can be drawn to.
	Params:
		reference =   - a reference to the wrapped SDL_Texture
++/
class Window
{
	SDL_Window* reference = null;

	/++
		Creates a window with a title name, a given width, height, an optional x position, and optional y position.
		Throws: WindowCreationError on failure
	++/
	this(string name, int width, int height, int xpos = SDL_WINDOWPOS_UNDEFINED, int ypos = SDL_WINDOWPOS_UNDEFINED)
	{
		reference = SDL_CreateWindow(cast(const char*) name, xpos, ypos, width, height, SDL_WINDOW_SHOWN);
		if (reference == null)
		{
			throw new Exception("WindowCreationError");
		}
	}
	///
	unittest
	{
		// Create a new window
		auto w = new Window("Window module unittest", 640, 480);
	}


	~this()
	{
		SDL_DestroyWindow(reference);
	}

	// FIXME: object properties should be lower camel-case. Lowercase the s please :)
	@property SDL_Surface* Surface() { return SDL_GetWindowSurface(reference); }

	/// Fill the window with a given color.
	void FillColor(RGB color)
	{
		SDL_FillRect(this.Surface, null, SDL_MapRGB(this.Surface.format, color.r, color.g, color.b));
	}
	///
	unittest
	{
		// Create a new window
		auto window = new Window("Window module unittest", 640, 480);

		// Fill the window with a nice dark grey
		window.FillColor(RGB(0x22, 0x22, 0x22));
	}

	/// Draw a given image to the window.
	void Draw(Image image)
	{
		SDL_Rect* loc = new SDL_Rect (image.position.x, image.position.y, image.size.x, image.size.y);
//		const(SDL_Rect)* clip = new SDL_Rect (0, 0, image.clip.x, image.clip.y);
//		auto loc = SDLifyRect(image.screenspace);
//		auto clip = ConstSDLifyRect(image.clip);
		SDL_BlitSurface(image.reference, image.region.SDLifyPtr, this.Surface, loc);
	}
	///
	unittest
	{
		import engine.image: Image;

		// Create a new window
		auto window = new Window("Window module unittest", 640, 480);

		// Create an image and ensure it exists
		auto i = new Image("data/image.png", window);

		// Draw the image. It won't actually show up untill we call window.Update() though!
		window.Draw(i);
	}

	/// Update the window's contents. Call this after you are done drawing everything.
	void Update()
	{
		SDL_UpdateWindowSurface(reference);
	}
	///
	unittest
	{
		import engine.image: Image;

		// Create a new window and make sure it exists
		auto window = new Window("Window module unittest", 640, 480);

		// Create two images, using the same image file :P
		auto i1 = new Image("data/image.png", window);
		auto i2 = new Image("data/image.png", window);

		// Move the second image a little bit
		i2.position = Vec2i(10, 10);

		// Draw the two images
		window.Draw(i1);
		window.Draw(i2);

		// Update the window with all newly drawn images
		window.Update();
	}
}
