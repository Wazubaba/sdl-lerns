module engine.core.util.string;

/++
	Simple wildcard match for strings when regex would be overkill.
	* will match anything up to the next char. See example for a better
	explanation.
	Returns: true if string matches, false otherwise
++/

version (verbose) import std.stdio: writeln, write, stdout;

bool WildCardMatch(string base, string query)
{
	int i = 0;
	bool escaped = false;

	foreach (c; base)
	{
		version (verbose){write("[", c, "]\t-"); stdout.flush();}
		if (i == query.length) return true;

		if (query[i] == '\\' && !escaped)
		{


			escaped = true;
			i++;
		}

		if (query[i] == '*' && !escaped)
		{
			version (verbose){writeln("[+] matching any!"); stdout.flush();}
			if (query[i+1] == c || query[i+1] == '\\')
				i += 2; // Skip next char since it is already matched
		}
		else
		{
			version (verbose){writeln("[+] Does '", c, "' == '", query[i], "' ?");}
			if (c != query[i])
				return false;
			i++;
		}
		escaped = false;
	}
	return true;
}
///
unittest
{
	string test = "hi there";

	// Match everything between the space and the r.
	assert(test.WildCardMatch("hi *re"));

	// Test if the string ends with rawr.
	assert(!test.WildCardMatch("*rawr"));

	test = "sample * text with a \\";

	// Now let's see an example of escaping..
	assert(test.WildCardMatch("*le \\* text"));// with a \\\\"));

	// And a failed escaped test?
	assert(!test.WildCardMatch("*\\* text with a taco"));
}
