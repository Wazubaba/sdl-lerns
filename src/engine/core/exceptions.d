module engine.core.exceptions;
import std.string: format;
import engine.cli: Perror, Print, Pinfo;

/++
	Exception when a file cannot be found.
++/
class FileLoadError: Exception
{
	this(string path, string additionalInfo)
	{
		Perror("Cannot load file '%s'\n\tAdditional Information: %s".format(path, additionalInfo));
		super("FileLoadError");
	}

	this(string path)
	{
		Perror("Cannot load file '%s'".format(path));
		super("FileLoadError");
	}
}

/++
	Exception thrown when a requested file does not exist.
++/
class FileNotFound: Exception
{
	this(string path, string additionalInfo)
	{
		Perror("Cannot find file '%s'\n\tAdditional Information: %s".format(path, additionalInfo));
		super("FileNotFound");
	}

	this(string path)
	{
		Perror("Cannot find file '%s'".format(path));
		super("FileNotFound");
	}
}

/++
	Exception thrown when a null texture is accessed.
++/
class UninitializedResourceError: Exception
{
	this()
	{
		Perror("Attemp to access an uninitialized resource");
		super("UninitializedResourceError");
	}
}

/++
	Exception thrown when a initialization of something fails, whether it's due to
	something else being messed up, or simple because of an error. This means basically
	that creating a new object just failed. Think of this as a more generic form of the
	exceptions that can happen.

	Untill I can find a better way of it, just set baseResourceClass to the type of class
	that throws this error.
++/
class ResourceInitializationError:Exception
{
	this(string baseResourceClass, string info)
	{
		Perror("Failed to initialize new %s\n\t Additional information: %s".format(baseResourceClass, info));
		super("%sInitializationError".format(baseResourceClass));
	}

	this(string baseResourceClass)
	{
		Perror("Failed to initialize new %s".format(baseResourceClass));
		super("%sInitializationError".format(baseResourceClass));
	}
}

/++
	Exception thrown when a sprite cannot be initialized.
++/
deprecated class SpriteCreationError: Exception
{
	this(string additionalInfo)
	{
		Perror("Error initializing sprite\n\tAdditional information: %s".format(additionalInfo));
		super("SpriteCreationError");
	}

	this()
	{
		Perror("Error initializing sprite");
		super("SpriteCreationError");
	}
}

deprecated("use ResourceInitializationError instead") class ImageCreationError: Exception
{
	this()
	{
		Perror("Error creating image from given path");
		super("ImageCreationError");
	}
}

/++
	Exception thrown when loader cannot find specified image.
++/
deprecated("use FileNotFound instead") class ImageNotFound: Exception
{
	this(string path)
	{
		super("Image not found: %s".format(path));
	}
}

/++
	Exception thrown when no image is loaded and a function needing the image is
	called.
++/
deprecated("use UninitializedResourceError instead") class ImageNotLoaded: Exception
{
	this()
	{
		super("Image not loaded");
	}
}

/++
	Exception thrown when converting the image to the proper RGB type for a
	given `Window` fails.
++/
class ConversionError: Exception
{
	this()
	{
		super("Cannot convert image");
	}
}

/++
	Exception thrown when creating a window fails.
++/
deprecated("use ResourceInitializationError instead") class WindowCreationError: Exception
{
	this(string additionalInfo)
	{
			super("Failed to create window. Additional info: %s".format(additionalInfo));
	}

	this()
	{
		super("Failed to create window");
	}
}

/++
	Exception thrown when creating a render context fails.
++/
deprecated("use ResourceInitializationError instead") class ContextCreationError: Exception
{
	this(string additionalInfo = "")
	{
		super("[ERR] Failed to create render context. Additional info: %s".format(additionalInfo));
	}

	this()
	{
		super("[ERR] Failed to create render context");
	}
}
