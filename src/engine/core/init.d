module engine.core.init;
import derelict.sdl2.sdl: SDL_WasInit, SDL_QuitSubSystem, SDL_Quit, DerelictSDL2, SDL_GetError, SDL_Init, SDL_INIT_EVERYTHING;
import derelict.sdl2.image: DerelictSDL2Image;
import derelict.sdl2.mixer: DerelictSDL2Mixer;
import derelict.sdl2.ttf: DerelictSDL2ttf, TTF_Init, TTF_GetError, TTF_Quit;
import derelict.sdl2.net: DerelictSDL2Net;
import derelict.lua.lua;

import std.string: fromStringz;

import engine.cli: Perror, Print, Pinfo;
//import engine.window: Window;

static string[] DATA_PATHS;

/// Ensure that SDL is properly shutdown when program ends.
shared static ~this()
{
	ShutdownSDL();
}

/// Unloads all loaded SDL modules and shuts down SDL. Get's called automatically on shutdown.
void ShutdownSDL()
{
	// Only will have loaded modules in this :P
	int loadedSystems = SDL_WasInit(SDL_INIT_EVERYTHING);
	SDL_QuitSubSystem(loadedSystems);
	TTF_Quit();
	SDL_Quit();
	Pinfo("SDL2 Shutdown");
}


/++
	Starts SDL using the given path to find libraries.
	Throws: InitErrorSDL on failure
++/
void StartSDL(string libPath, string libName)
{
	import std.path: buildPath;
	scope(success) Print(0, "\tSDL2: \x1b[1;32mOK\x1b[0m");
	scope(failure) Print(0, "\tSDL2: \x1b[1;41mFAIL\x1b[0m");

	// Load all of the things. Yes.
	version (linux)
	{
		DerelictSDL2.load(buildPath(libPath, libName));
		DerelictSDL2Image.load(buildPath(libPath, "libSDL2_image.so"));
		DerelictSDL2Mixer.load(buildPath(libPath, "libSDL2_mixer.so"));
		DerelictSDL2ttf.load(buildPath(libPath, "libSDL2_ttf.so"));
		DerelictSDL2Net.load(buildPath(libPath, "libSDL2_net.so"));
	}
	version (Windows)
	{
		DerelictSDL2.load(buildPath(libPath, libName));
		DerelictSDL2Image.load(buildPath(libPath, "SDL2_image.dll"));
		DerelictSDL2Mixer.load(buildPath(libPath, "SDL2_mixer.dll"));
		DerelictSDL2ttf.load(buildPath(libPath, "SDL2_ttf.dll"));
		DerelictSDL2Net.load(buildPath(libPath, "SDL2_net.dll"));
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		Perror(0, "SDL Could not intialize all modules! Error: ", SDL_GetError());
		throw new Exception("InitErrorSDL");
	}

	if (TTF_Init() < 0)
	{
		Perror(0, "TTF could not initialize! Error: ", TTF_GetError().fromStringz);
		throw new Exception("InitErrorSDL-TTF");
	}
}

/// Starts Lua using the given path to find libraries.
void StartLua(string libPath, string libName)
{
	import std.path: buildPath;
	scope(success) Print(0, "\tLua: \x1b[1;32mOK\x1b[0m");
	scope(failure) Print(0, "\tLua: \x1b[1;41mFAIL\x1b[0m");

	DerelictLua.load(buildPath(libPath, libName));
}

/++
	Initializes the engine and all subsystems in a cross-platform manner.
	You $(B MUST) call this before anything will work!
++/
void StartEngine()
{
	import std.path: buildPath;

	// This weird bit ensures the correct libraries get loaded for the right os
	// and cpu arch
	string platform;
	string sdlLibName;
	string luaLibName;

	version (linux){ platform = "lin"; sdlLibName = "libSDL2.so.2.0.4"; luaLibName = "liblua.so.5.3.2"; }
	version (Windows){ platform = "win"; sdlLibName = "SDL2.dll"; luaLibName = "lua53.dll"; }
	version (Darwin) platform = "osx";

	version (X86_64) platform ~= "64";
	version (X86) platform ~= "32";

	// XXX: Change this to alter where libraries are to be found
	string libPath = buildPath("support", platform);

	Pinfo("Initializing subsystems..");
	try StartSDL(libPath, sdlLibName);
	catch (Exception e)
	{
		Perror("[ERROR] Cannot initialized SDL2!");
		throw e;
	}

	try StartLua(libPath, luaLibName);
	catch (Exception e)
	{
		Perror("[ERROR] Cannot initialize Lua!");
		throw e;
	}

	Pinfo("Complete");
}
///
unittest
{
	// Yup :D
	StartEngine();
}

/++
	Deprecated. Pending reimplementation due to changes under the hood.
	TODO: Make this use the new vfs module.
++/
void SetDataPaths(string[] paths)
{
	DATA_PATHS = paths;
}
///
unittest
{
	// Wheeee
	SetDataPaths(["data", "subdata"]);
}
