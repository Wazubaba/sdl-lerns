module engine.core.event;
import derelict.sdl2.sdl: SDL_QUIT, SDL_WINDOWEVENT, SDL_SYSWMEVENT,
SDL_KEYDOWN, SDL_KEYUP, SDL_TEXTEDITING, SDL_TEXTINPUT, SDL_KEYMAPCHANGED,
SDL_MOUSEMOTION, SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP, SDL_MOUSEWHEEL,
SDL_JOYAXISMOTION, SDL_JOYBALLMOTION, SDL_JOYHATMOTION,  SDL_JOYBUTTONDOWN,
SDL_JOYBUTTONUP, SDL_JOYDEVICEADDED, SDL_JOYDEVICEREMOVED,
SDL_CONTROLLERAXISMOTION, SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLERBUTTONUP,
SDL_CONTROLLERDEVICEADDED, SDL_CONTROLLERDEVICEREMOVED,
SDL_CONTROLLERDEVICEREMAPPED, SDL_FINGERDOWN, SDL_FINGERUP, SDL_FINGERMOTION,
SDL_DOLLARGESTURE, SDL_DOLLARRECORD, SDL_MULTIGESTURE, SDL_CLIPBOARDUPDATE,
SDL_DROPFILE, SDL_AUDIODEVICEADDED, SDL_AUDIODEVICEREMOVED,
SDL_RENDER_TARGETS_RESET, SDL_RENDER_DEVICE_RESET, SDL_Event, SDL_PollEvent;
//import derelict.sdl2.sdl;
/++
	Contains functions for identifying events and working with them.
++/

enum EventType
{
	// Application events
	quit = SDL_QUIT,

	// Window events
	window = SDL_WINDOWEVENT,
	windowManager = SDL_SYSWMEVENT,

	// Keyboard events
	keyDown = SDL_KEYDOWN,
	keyUp = SDL_KEYUP,
	textEditing = SDL_TEXTEDITING,
	textInput = SDL_TEXTINPUT,
	keymapChanged = SDL_KEYMAPCHANGED,

	// Mouse events
	mouseMotion = SDL_MOUSEMOTION,
	mouseButtonDown = SDL_MOUSEBUTTONDOWN,
	mouseButtonUp = SDL_MOUSEBUTTONUP,
	mouseWheel = SDL_MOUSEWHEEL,

	// Joystick events
	joystickAxis = SDL_JOYAXISMOTION,
	joystickTrackBall = SDL_JOYBALLMOTION,
	joystickHat = SDL_JOYHATMOTION,
	joystickButtonDown = SDL_JOYBUTTONDOWN,
	joystickButtonUp = SDL_JOYBUTTONUP,
	joystickAdded = SDL_JOYDEVICEADDED,
	joystickRemoved = SDL_JOYDEVICEREMOVED,

	// Controller events
	controllerAxis = SDL_CONTROLLERAXISMOTION,
	controllerButtonDown = SDL_CONTROLLERBUTTONDOWN,
	controllerButtonUp = SDL_CONTROLLERBUTTONUP,
	controllerAdded = SDL_CONTROLLERDEVICEADDED,
	controllerRemoved = SDL_CONTROLLERDEVICEREMOVED,
	controllerKeymapChanged = SDL_CONTROLLERDEVICEREMAPPED,

	// Touch events
	touchDown = SDL_FINGERDOWN,
	touchUp = SDL_FINGERUP,
	touchMotion = SDL_FINGERMOTION,

	// Gesture events
	gesture = SDL_DOLLARGESTURE,
	gestureRecord = SDL_DOLLARRECORD,
	multipleGesture = SDL_MULTIGESTURE,

	// Clipboard events
	clipboardUpdate = SDL_CLIPBOARDUPDATE,

	// Drag and Drop events
	fileDropped = SDL_DROPFILE,

	// Audio Hotplug events
	audioDeviceAdded = SDL_AUDIODEVICEADDED,
	audioDeviceRemoved = SDL_AUDIODEVICEREMOVED,

	// Render events
	renderTargetReset = SDL_RENDER_TARGETS_RESET,
	renderDeviceReset = SDL_RENDER_DEVICE_RESET,

	// User events
	// XXX: Fill this out with extra events you may need :D
	// TODO: Figure out a way to wrap around SDL_RegisterEvents()
};

/++
	Simple wrapper around SDL_Event.
++/
struct EventListener
{
	SDL_Event info;

	/// Get the type of the last event to be fired.
	@property uint type() { return this.info.type; }

	/++
		Get the next event to process.
		Returns true untill all events are handled.
	++/
	bool Next()
	{
		if (SDL_PollEvent(&this.info) != 0)
			return true;
		else
			return false;
	}
}
