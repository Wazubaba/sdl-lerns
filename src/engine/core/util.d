module engine.core.util;

import derelict.sdl2.sdl;
import engine.core.types;
deprecated
{
/++
	For now this is Deprecated.
	Contains numerous utility functions, including type conversion functions.
++/

/++
	Convert a Rect to an SDL_Rect*.
	Returns: SDL_Rect* result
++/
SDL_Rect* SDLifyRect(ref Rect rect)
{
	return new SDL_Rect(rect.x, rect.y, rect.w, rect.h);
}

/++
	Convert a Rect to a const(SDL_Rect*).
	Returns: const(SDL_Rect*) result
++/
const(SDL_Rect*) ConstSDLifyRect(ref Rect rect)
{
	return new SDL_Rect(rect.x, rect.y, rect.w, rect.h);
//	const auto rval = new SDL_Rect(rect.x, rect.y, rect.w, rect.h);
//	return rval;
}

}
