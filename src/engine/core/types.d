module engine.core.types;

import derelict.sdl2.sdl;

// TODO: Go through this and purge anything that isn't needed

struct Rect
{
	int x;
	int y;
	int w;
	int h;

	@property SDL_Rect SDLify() { return SDL_Rect(x, y, w, h); }
	@property const SDL_Rect* SDLifyPtr() { return new SDL_Rect(x, y, w, h); }

	@property Vec2i position() { return Vec2i(x, y); }
	@property void position(Vec2i pos) { x = pos.x; y = pos.y; }

	@property Vec2i size() { return Vec2i(w, h); }
	@property void size(Vec2i size) { w = size.x; h = size.y; }
}

struct Vec2i
{
	int x;
	int y;
}

struct Vec2f
{
	float x;
	float y;
}

struct RGB
{
	ubyte r;
	ubyte g;
	ubyte b;

	@property SDLify() { return SDL_Color(r, g, b); }
	@property const SDLify() { return SDL_Color(r, g, b); }
}

struct RGBA
{
	ubyte r;
	ubyte g;
	ubyte b;
	ubyte a;
}

/// All drawable entities must inherit from this
interface Drawable
{
	@property SDL_Surface* Surface();
	@property Vec2i Position();
//	@property Vec2i Scale(); // May not be needed?
}

/// All renderable classes should inherit from this to work with the engine's renderering system;
interface Renderable
{
	@property SDL_Texture* Reference();
	@property Vec2i Position();
	@property void Position(Vec2i newPosition);
	@property Vec2i Size();
	@property void Size(Vec2i newSize);
	@property Vec2i Clip();
	@property void Clip(Vec2i newClip);
	@property Vec2i ClipSize();
	@property void ClipSize(Vec2i newClipSize);
}

/// All drawable targets must inherit from this
interface View
{
	@property SDL_Surface* Surface();
	@property SDL_Window* Window();
	void Blit(Drawable sprite);
	void Update();
}
