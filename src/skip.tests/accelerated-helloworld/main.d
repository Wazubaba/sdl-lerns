import engine;
import derelict.sdl2.sdl: SDL_Event, SDL_PollEvent, SDL_QUIT;

int main(string[] args)
{
	bool quit = false;
	SDL_Event e;

	Pinfo("Starting engine!");
	StartEngine();

	Pinfo("Creating window!");
	Window window = new Window("SDL Tutorial", 640, 480);
	// SetAsRootWindow(window);

	assert(window.Window != null);

	Pinfo("Creating renderer!");
	Renderer renderer = new Renderer(window);
//	SetAsRootRenderer(renderer);
//	renderer.AssignWindow(window);

	if (window.Window == null) Pwarn("LOST WINDOW!");

	SetDataPaths(["data"]);

	Pinfo("Loading image!");
	Image image = new Image("image.png");



	Pinfo("Creating texture!");
	Texture texture = new Texture("image.png", renderer);

	renderer.SetDrawColor(RGBA(0x22, 0x22, 0x22, 0xFF));
	renderer.Clear();

	while (!quit)
	{
		renderer.SetDrawColor(RGBA(0x22, 0x22, 0x22, 0x));
		renderer.Clear();

		while(SDL_PollEvent(&e) != 0)
		{
			// User requests quit
			if (e.type == SDL_QUIT)
				quit = true;
		}
		renderer.Draw(texture);
		renderer.Update();
	}

	return 0;
}
/+
string[] DATA_PATHS = ["data"];
SDL_Renderer* gRenderer = null;
SDL_Texture* gTexture = null;
SDL_Surface* gSurface = null;

SDL_Texture* Load2(const string path)
{
	import std.path: buildPath;
	import std.stdio: writeln;

	SDL_Texture* texture = null;
	SDL_Surface* image = null;

	foreach (dataPath; DATA_PATHS)
	{
		string fullPath = buildPath(dataPath, path);
		image = IMG_Load(cast(const char*) fullPath.dup);
		if (image)
		{
			import std.string: format;
			Info("Loaded image: %s".format(fullPath));
			break;
		}
	}

	if (!image)
	{
		import std.string: format;
		Perror("Cannot find %s in any of %s".format(path, DATA_PATHS));
		throw new Exception("ImageNotFound");
	}

	texture = SDL_CreateTextureFromSurface(gRenderer, image);
	if (!texture)
		throw new Exception("TextureCreationError");

	SDL_FreeSurface(image);
	return texture;
}

//
// SDL_Texture* Load2(const string path)
// {
// 	import std.path: buildPath, absolutePath;
//
// 	SDL_Texture* texture = null;
//
// 	Perror(absolutePath(buildPath(DATA_PATH, path)));
// 	SDL_Surface* loadedSurface = IMG_Load(cast(const char*) buildPath(DATA_PATH, path));
// 	if (!loadedSurface)
// 		throw new Exception("ImageNotFound");
//
// 	texture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
// 	if (!texture)
// 		throw new Exception("TextureCreationError");
//
// 	SDL_FreeSurface(loadedSurface);
// 	return texture;
// }


/*
int main(string[] args)
{
	import std.path: buildPath;

	bool quit = false;

	SDL_Event e;

	InitEngine();

	LoadScriptFromFile(buildPath(DATA_PATHS[0], "script.lua"));

	Window window = new Window("SDL Tutorial", 640, 480);

	scope(exit)
	{
		SDL_FreeSurface(gSurface);
		SDL_DestroyTexture(gTexture);
		delete(gRenderer);
		delete(gTexture);
		delete(window);
		IMG_Quit();
		SDL_Quit();
	}

	gRenderer = SDL_CreateRenderer(window.Window, -1, SDL_RENDERER_ACCELERATED);
	if (!gRenderer)
	{
		writeln("[ERROR] ", SDL_GetError());
		return -1;
	}

	SDL_SetRenderDrawColor(gRenderer, 0x22, 0x22, 0x22, 0xFF);

	// init png loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		writeln("[ERROR] ", IMG_GetError());
		return -1;
	}

	gTexture = Load2("image.png");
	while (!quit)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
				quit = true;
		}

		// Set Clear color
		SDL_SetRenderDrawColor(gRenderer, 0x22, 0x22, 0x22, 0xFF);

		// Clear screen
		SDL_RenderClear(gRenderer);

		// ACTUALLY DRAW THE FUCKING IMAGE YES
		SDL_RenderCopy(gRenderer, gTexture, null, null);
/*
		// Render texture to screen
		SDL_RenderCopy(gRenderer, gTexture, null, null);

		int SCREEN_WIDTH, SCREEN_HEIGHT;
		SDL_GetRendererOutputSize(gRenderer, &SCREEN_WIDTH, &SCREEN_HEIGHT);

		// Draw a red box places
		SDL_Rect fillRect = { SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2};
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0x00, 0x00, 0xFF);
		SDL_RenderFillRect(gRenderer, &fillRect);

		// Draw a green box outline places around red box places
		SDL_Rect outlineRect = { SCREEN_WIDTH / 6, SCREEN_HEIGHT / 6, SCREEN_WIDTH * 2 / 3, SCREEN_HEIGHT * 2 / 3};
		SDL_SetRenderDrawColor(gRenderer, 0x00, 0xFF, 0x00, 0xFF);
		SDL_RenderDrawRect(gRenderer, &outlineRect);

		// Draw a solid blue horizontal line
		SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0xFF, 0xFF);
		SDL_RenderDrawLine(gRenderer, 0, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT / 2);

		// Draw a dotted yellow vertical line
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0x00, 0xFF);
		for (int i = 0; i < SCREEN_HEIGHT; i += 4)
			SDL_RenderDrawPoint(gRenderer, SCREEN_WIDTH / 2, i);
*/

		// Update screen
		SDL_RenderPresent(gRenderer);
	}

/*
	Sprite sprite = new Sprite("image.png", window);

	Sprite upArrow = new Sprite("res/arrow-up.png", window);
	Sprite leftArrow = new Sprite("res/arrow-left.png", window);
	Sprite rightArrow = new Sprite("res/arrow-right.png", window);
	Sprite downArrow = new Sprite("res/arrow-down.png", window);

	Sprite current = new Sprite();

	window.FillColor(RGB(0x22, 0x22, 0x22));

	window.Blit(sprite);
	window.Update();

	while(!quit)
	{
		// Clear the screen to dark grey
		window.FillColor(RGB(0x22, 0x22, 0x22));

		while(SDL_PollEvent(&e) != 0)
		{
			// User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			} else
			if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
					case SDLK_UP:
						current = upArrow; break;
					case SDLK_DOWN:
						current = downArrow; break;
					case SDLK_LEFT:
						current = leftArrow; break;
					case SDLK_RIGHT:
						current = rightArrow; break;
					default:
						current = sprite; break;
				}
			}

		}
		window.Blit(current);
		window.Update();
	}

	//SDL_Delay(2000);

	// Release the window from memory
	delete(window);
*/
//	SDL_Quit();

	return 0;
}
+/
