import engine;
import derelict.sdl2.sdl: SDL_Event, SDL_PollEvent, SDL_QUIT;

int main(string[] args)
{
	bool quit = false;
	SDL_Event e;

	StartEngine();

	Window window = new Window("SDL Tutorial", 640, 480);
	window.SetAsRootWindow();

	SetDataPaths(["data"]);

	Pinfo("Loading image!");
	Image image = new Image("image.png");

	while (!quit)
	{
		window.FillColor(RGB(0x22, 0x22, 0x22));

		while(SDL_PollEvent(&e) != 0)
		{
			// User requests quit
			if (e.type == SDL_QUIT)
				quit = true;
		}

		window.Draw(image);
		window.Update();
	}

	return 0;
}
