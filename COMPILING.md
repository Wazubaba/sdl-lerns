# COMPILING #
Compiling is simple, provided you have dmd and make properly installed.
Issue `make` to build. You can also specify `make -jn` where n is the number of
worker threads you want. If you omit the number it will just make as many as
it possibly can, resulting in the fastest possible build.
