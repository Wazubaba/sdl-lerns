DC := dmd
BIN := sdl-test

# Ensure that the entire thing is built
.DEFAULT_GOAL:=all

# Import correct buildscript for platform
ifeq ($(shell uname -o), Cygwin)
	include buildscripts/cygwin.mak
	include buildscripts/libraries-windows.mak
endif

ifeq ($(shell uname -o), linux)
	include buildscripts/linux.mak
	include buildscripts/libraries-linux.mak
endif

ifeq ($(shell uname -o), GNU/Linux)
	include buildscripts/linux.mak
	include buildscripts/libraries-linux.mak
endif

INCLUDES := $(LINK_PATH) -Iinclude -L-Llib  -Isrc

DFLAGS := -w -g $(INCLUDES)

# Detect source files
PROJDIRS := src

AUXFILES := Makefile README.md
SRCFILES := $(shell find $(PROJDIRS) -type f ! -name "*skip.*" -and ! -path "*skip.*" -name "*.d")
OBJFILES := $(patsubst %.d, %.$(PLATFORM_OBJECT_TYPE), $(SRCFILES))
DOCFILES := $(patsubst %.d, docs/%.html, $(SRCFILES))
ALLFILES := $(SRCFILES) $(AUXFILES)

.PHONY: all install uninstall clean todolist dist libs docs clean-libs clean-docs clean-full platform_install platform_uninstall

all: libs bin/$(BIN)

# Build objects
%.$(PLATFORM_OBJECT_TYPE): %.d
	$(DC) $(DFLAGS) $(INCLUDES) -c $< -of$@

# Output binary
bin/$(BIN): $(OBJFILES) libs
	@mkdir -p bin
	$(DC) -of$@ $(OBJFILES) $(DFLAGS) $(LIBS)
	ln -sf bin/$(BIN) $(BIN)

# Call installation for given platform
install: platform_install

# Call deinstallation for given platform
uninstall: platform_uninstall

# Clear out generated project build files
clean:
	@-$(RM) $(wildcard $(OBJFILES) $(DEPFILES)) $(BIN) -r release bin

# Clear only generated library build files
clean-libs:
	@-$(RM) lib/*.a lib/*.lib *.dll

# Clear only generated documentation
clean-docs:
	@-$(RM) -r docs/src

# Clear all generated files
clean-full: clean-libs clean-docs clean

# Generate a distribution archive - WARNING: Probably only works on linux
dist:
	@echo "Generating package..."
	@-mkdir -p release
	@-strip bin/$(BIN)
	@-upx -q --lzma --best bin/$(BIN)
	@tar czf release/$(BIN).tar.gz $(ALLFILES)
	@tar cjf release/$(BIN).tar.bz2 $(ALLFILES)

## Utility rules ##
todolist:
	-@for file in $(ALLFILES:Makefile=); do fgrep -H -e TODO -e FIXME -e XXX $$file; done; true

# Generate html files
docs/%.html: %.d
	@mkdir -p docs
	$(DC) -D -Dddocs -Df$@ -o- -c -unittest $< $(DFLAGS) $(LIBS)

# Generate documentation
docs: $(DOCFILES)
